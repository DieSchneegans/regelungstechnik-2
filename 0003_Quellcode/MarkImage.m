function [r1,g1,b1] = MarkImage(r,g,b, pos, detect)
%#codegen
r1 = r; g1 = g; b1 = b;
if detect
    g1(:, pos(2)-3:pos(2)+3) = 255;
    g1(pos(1)-3:pos(1)+3, :) = 255;
    r1(pos(1)-3:pos(1)+3, pos(2)-3:pos(2)+3) = 0;
    b1(pos(1)-3:pos(1)+3, pos(2)-3:pos(2)+3) = 0;
end