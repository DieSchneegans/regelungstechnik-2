function [pos, detect] = FindCentroid(bw)
%#codegen
[r,c]=find(bw);
if isempty(r)
    pos = [-1 -1];
    detect = false;
else
    pos = [mean(r), mean(c)];
    detect = true;
end
