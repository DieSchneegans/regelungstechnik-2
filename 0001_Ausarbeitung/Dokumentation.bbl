\begin{thebibliography}{6}
% this bibliography was produced with the style dinat.bst v2.5
\makeatletter
\newcommand{\dinatlabel}[1]%
{\ifNAT@numbers\else\NAT@biblabelnum{#1}\hspace{2\labelsep}\fi}
\makeatother
\expandafter\ifx\csname natexlab\endcsname\relax\def\natexlab#1{#1}\fi
\expandafter\ifx\csname url\endcsname\relax\def\url#1{\texttt{#1}}\fi

\bibitem[B{\"u}hler u.\,a.(2017)B{\"u}hler, Schlaich und Sinner]{Buhler.2017}
\dinatlabel{B{\"u}hler u.\,a. 2017} \textsc{B{\"u}hler}, Peter~;
  \textsc{Schlaich}, Patrick~; \textsc{Sinner}, Dominik:
\newblock \emph{Digitales Bild: Bildgestaltung - Bildbearbeitung -
  Bildtechnik}.
\newblock Berlin~: {Springer Vieweg}, 2017
\newblock (Bibliothek der Mediengestaltung). --
\newblock URL \url{http://www.springer.com/}. --
\newblock ISBN 978-3-662-53893-7

\bibitem[Duden(14. Mai. 2021{\natexlab{a}})]{Duden.Bilddaten}
\dinatlabel{Duden 14. Mai. 2021{\natexlab{a}}} \textsc{Duden}:
\newblock \emph{Bilddaten}.
\newblock 14. Mai. 2021. --
\newblock URL \url{https://www.duden.de/rechtschreibung/Bilddaten}. --
  Zugriffsdatum: 14. Mai. 2021

\bibitem[Duden(14. Mai. 2021{\natexlab{b}})]{Duden.Bildverarbeitung}
\dinatlabel{Duden 14. Mai. 2021{\natexlab{b}}} \textsc{Duden}:
\newblock \emph{Bildverarbeitung}.
\newblock 14. Mai. 2021. --
\newblock URL \url{https://www.duden.de/rechtschreibung/Bildverarbeitung}. --
  Zugriffsdatum: 14. Mai. 2021

\bibitem[Erhardt(2008)]{Erhardt.2008}
\dinatlabel{Erhardt 2008} \textsc{Erhardt}, Angelika:
\newblock \emph{Einf{\"u}hrung in die digitale Bildverarbeitung: Grundlagen,
  Systeme und Anwendungen ; mit 35 Beispielen und 44 Aufgaben}.
\newblock 1. Aufl.
\newblock Wiesbaden~: {Vieweg + Teubner}, 2008
\newblock (Studium). --
\newblock ISBN 978-3-519-00478-3

\bibitem[Nischwitz u.\,a.(2020)Nischwitz, Fischer, Haber{\"a}cker und
  Socher]{Nischwitz.2020}
\dinatlabel{Nischwitz u.\,a. 2020} \textsc{Nischwitz}, Alfred~;
  \textsc{Fischer}, Max~; \textsc{Haber{\"a}cker}, Peter~; \textsc{Socher},
  Gudrun:
\newblock \emph{Bildverarbeitung: Band II des Standardwerks Computergrafik und
  Bildverarbeitung}.
\newblock 4th ed. 2020.
\newblock Wiesbaden~: {Springer Fachmedien Wiesbaden} and {Imprint: Springer
  Vieweg}, 2020. --
\newblock ISBN 978-3-658-28705-4

\bibitem[Werner(2021)]{Werner.2021}
\dinatlabel{Werner 2021} \textsc{Werner}, Martin:
\newblock \emph{Digitale Bildverarbeitung: Grundkurs mit neuronalen Netzen und
  MATLAB{\circledR}-Praktikum}.
\newblock 1st ed. 2021.
\newblock Wiesbaden~: {Springer Fachmedien Wiesbaden} and {Imprint: Springer
  Vieweg}, 2021. --
\newblock ISBN 978-3-658-22185-0

\end{thebibliography}
